import csv
import random
import sys
import tkinter as tk
from tkinter.filedialog import askopenfilename

class Card:
    def __init__(self, i, j, parent,
        text="",
        question="",
        answer="",
        daily_double=False,
        category=False,
        points=0,
    ):
        self.parent = parent
        self.text = text
        self.question = question
        self.points = points*2 if daily_double else points
        self.daily_double = daily_double
        self.i = i
        self.j = j
        self.state = 0
        self.category = category

    def render(self, parent=None):
        if not parent is None:
            self.parent = parent

        frame = tk.Frame(
            master=self.parent,
            relief=tk.RIDGE,
            borderwidth=2,
            background="purple",
        )
        frame.bind("<Button-1>", self.click)

        self.label = tk.Label(
            master=frame,
            text=self.text,
            font=("Helvetica bold", 25),
            foreground="white" if self.category else "yellow",
            background="purple",
        )
        self.label.bind("<Configure>", lambda e: self.label.config(wraplength=self.frame.winfo_width()))
        self.label.bind("<Button-1>", self.update)

        self.label.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

    def click(self, event=None):
        self.state += 1
        if self.state == 1:
            self.text = self.question
        elif self.state == 2:
            self.text = self.answer
        else:
            self.text = f"{self.i*100}"

        self.render()

class Cell:
    def __init__(self, window, i, j,
            text="",
            points=0,
            category=False,
            question="",
            answer="",
            daily_double=False,
            update_func=None,
            display_func=None,
        ):
        self.window = window
        self.state = 0
        self.question = question
        self.answer = answer
        self.text = text
        self.points = points*2 if daily_double else points
        self.is_category = category
        self.is_daily_double = daily_double
        self.dd_displayed = False
        self.update_func = update_func
        self.display_func = display_func
        self.i = i
        self.j = j

        self.frame = tk.Frame(
            master=self.window,
            relief=tk.RIDGE,
            borderwidth=2,
            background="purple",
        )
        self.frame.grid(row=i, column=j, padx=2, pady=2, sticky="nsew")
        self.frame.bind("<Button-1>", self.update)

        self.label = tk.Label(
            master=self.frame,
            text=text,
            font=("Helvetica bold", 25),
            foreground="yellow" if not category else "white",
            background="purple",
        )
        self.label.bind("<Configure>", lambda e: self.label.config(wraplength=self.frame.winfo_width()))
        self.label.bind("<Button-1>", self.update)

    def render(self):
        self.label.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

    def update(self, event=None):
        if self.is_category:
            return
        if self.is_daily_double and not self.dd_displayed:
            #self.label.config(text="DAILY DOUBLE!")
            self.display_func("DAILY DOUBLE!!", self.i, self.j, False)
            self.dd_displayed = True
            return

        if self.state == 0:
            self.display_func(self.question, self.i, self.j, False)
            #self.label.config(text=self.question)
            self.state+=1
        elif self.state == 1:
            self.update_func(self.points)
            self.display_func(self.answer, self.i, self.j, True)
            #self.label.config(text=self.answer)
            self.state+=1
        else:
            self.label.config(text=self.text, foreground="gray")

class Jeopardy:
    def __init__(self, title, categories, questions, answers, teams=3):
        random.seed()
        self.title = title
        self.window = tk.Tk()
        self.window.title(self.title)
        self.categories = categories
        self.questions = questions
        self.answers = answers
        self.teams = [0] * teams
        self.current_points = 0
        self.board_x = len(self.categories)
        self.board_y = len(self.questions) + 1
        self.cells = []

        self.menu = tk.Frame(master=self.window, height=50, bg="gray")
        self.build_menu()
        self.display_board = tk.Frame(master=self.window)
        self.display_board_setup()
        self.board = tk.Frame(master=self.window)
        self.board_setup()
        self.scoreboard = tk.Frame(master=self.window, height=50, bg="gray")
        self.build_scoreboard()

    def reset_teams(self, entry):
        def fn():
            self.teams = [0] * int(entry.get())
            self.build_scoreboard()
        return fn

    def build_menu(self):
        import_button = tk.Button(master=self.menu, text="Open CSV", command=self.handle_import_csv)
        team_entry = tk.Entry(master=self.menu, width=4)
        team_entry.insert(0, "3")
        team_button = tk.Button(master=self.menu, text="Reset Teams", command=self.reset_teams(team_entry))
        quit_button = tk.Button(master=self.menu, text="Quit", command=self.quit)

        import_button.pack(side=tk.LEFT)
        team_button.pack(side=tk.LEFT)
        team_entry.pack(side=tk.LEFT)
        quit_button.pack(side=tk.RIGHT)
        self.menu.pack(fill=tk.X)

    def update_score(self, i, label):
        def update():
            self.teams[i] += self.current_points
            label.config(text=f"{self.teams[i]}")
            self.current_points = 0

        return update

    def minus_score(self, i, label):
        def minus():
            self.teams[i] -= 100
            label.config(text=f"{self.teams[i]}")
        return minus

    def build_scoreboard(self):
        for child in self.scoreboard.winfo_children():
            child.destroy()

        for i in range(len(self.teams)):
            outer_frame = tk.Frame(master=self.scoreboard)
            frame = tk.Frame(master=outer_frame)
            team_label = tk.Label(master=outer_frame, text=f"Team {i+1}")
            points_label = tk.Label(master=frame, text=f"{self.teams[i]}")
            button1 = tk.Button(master=frame, text="-", command=self.minus_score(i, points_label))
            button2 = tk.Button(master=frame, text="+", command=self.update_score(i, points_label))

            button1.pack(side=tk.LEFT)
            points_label.pack(side=tk.LEFT)
            button2.pack(side=tk.LEFT)

            team_label.pack()
            frame.pack()

            outer_frame.pack(side=tk.LEFT)

        self.scoreboard.pack(fill=tk.X)

    def handle_import_csv(self):
        filepath = askopenfilename(
            filetypes=[("CSV Files", "*.csv"), ("All Files", "*.*")]
        )
        if not filepath:
            return
        self.importCSV(filepath)

    def set_current_points(self, points):
        self.current_points = points

    def display_board_setup(self):
        self.display_frame = tk.Frame(
            master=self.display_board,
            relief=tk.RIDGE,
            borderwidth=2,
            background="purple",
        )
        self.display_label = tk.Label(
            master=self.display_frame,
            #text=text,
            font=("Helvetica bold", 25),
            foreground="yellow",
            background="purple",
        )
        self.display_label.place(relx=0.5, rely=0.5, anchor=tk.CENTER)
        self.display_frame.pack(fill=tk.BOTH, expand=True)


    def board_display(self, text, i, j, last):
        self.board.pack_forget()
        self.display_label.config(text=text)

        if not last:
            frame.bind("<Button-1>", self.cells[i][j].update)
            label.bind("<Button-1>", self.cells[i][j].update)
        else:
            frame.bind("<Button-1>", self.board_render)
            label.bind("<Button-1>", self.board_render)

        self.display_board.pack(fill=tk.BOTH, expand=True)


    def board_setup(self):
        for child in self.board.winfo_children():
            child.destroy()

        daily = (random.randrange(1,len(self.questions))+1, random.randrange(0,len(self.categories)))
        for i in range(self.board_y):
            self.board.rowconfigure(i, weight=1, minsize=100)

            row = []
            for j in range(self.board_x):
                self.board.columnconfigure(j, weight=1, minsize=100)
                if i == 0:
                    c = Cell(
                        self.board, i, j,
                        text=self.categories[j],
                        category=True,
                    )
                    row.append(c)
                    continue

                c = Cell(
                    self.board, i, j,
                    question=self.questions[i-1][j],
                    answer=self.answers[i-1][j],
                    points=i*100,
                    text=f"{i*100}",
                    daily_double=True if daily[0]==i and daily[1]==j else False,
                    update_func=self.set_current_points,
                    display_func=self.board_display,
                )
                row.append(c)
            self.cells.append(row)

        self.board_render()

    def board_render(self, event=None):
        self.display_board.pack_forget()

        for i in range(self.board_y):
            self.board.rowconfigure(i, weight=1, minsize=100)

            for j in range(self.board_x):
                self.board.columnconfigure(j, weight=1, minsize=100)
                self.cells[i][j].render()

        self.board.pack(fill=tk.BOTH, expand=True)


    def quit(self):
        sys.exit(0)

    def importCSV(self, filename):
        print(filename)
        rows = []
        with open(filename) as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                rows.append(row)

        if len(rows) <= 0:
            raise Exception

        self.categories = rows[0]

        self.questions = []
        self.answers = []
        self.teams = [0] * len(self.teams)
        i=1
        while i < len(rows):
            print(rows[i])
            if i % 2 == 1:
                self.questions.append(rows[i])
            else:
                self.answers.append(rows[i])
            i+=1

        self.board_setup()
        self.build_scoreboard()

    def render(self):
        self.window.mainloop()

def main():
    game = Jeopardy(
        "Jeopardy game",
        ["Edible Foods", "Again and again and again", "History 101"],
        [
            ["Q1A", "Q1B", "Q1C"],
            ["Q2A", "Q2B", "Q2C"],
            ["Q3A", "Q3B", "Q3C"],
            ["Q4A", "Q4B", "Q4C"],
            ["Q5A", "Q5B", "Q5C"],
        ],
        [
            ["A1A", "A1B", "A1C"],
            ["A2A", "A2B", "A2C"],
            ["A3A", "A3B", "A3C"],
            ["A4A", "A4B", "A4C"],
            ["A5A", "A5B", "A5C"],
        ],
    )
    game.render()

if __name__ == "__main__":
    main()
